" Buffers
map ]b :bnext<CR>
map [b :bprevious<CR>
map ]B :blast<CR>
map [B :bfirst<CR>

" Tabs
map ]a :tabnext<CR>
map [a :tabprevious<CR>
map ]A :tablast<CR>
map [A :tabfirst<CR>

" Location List
map ]l :lnext<CR>
map [l :lprev<CR>
map ]L :llast<CR>
map [L :lfirst<CR>

" Error List
map ]c :cnext<CR>
map [c :cprevious<CR>
map ]C :clast<CR>
map [C :cfirst<CR>

" Tags
map ]t :tnext<CR>
map [t :tprevious<CR>
map ]T :tlast<CR>
map [T :tfirst<CR>

" Folds
map ]z zr
map [z zm
map ]Z zR
map [Z zM

" Git merge conflict markers
map ]g /^<<<<<<< <CR>
map [g ?^<<<<<<< <CR>
map [G gg]g
map ]G G[g

" Toggles
map ]]a :$tabnew %<CR>
map [[a :tabclose<CR>

map ]]l :lwindow<CR>
map [[l :lclose<CR>

map ]]c :cwindow<CR>
map [[c :cclose<CR>

map ]]n :set number<CR>
map [[n :set nonumber<CR>

map ]]r :set relativenumber<CR>
map [[r :set norelativenumber<CR>

map ]]i :set ignorecase<CR>
map [[i :set noignorecase<CR>

map ]]s :syntax on<CR>
map [[s :syntax off<CR>

map ]]w :set list<CR>
map [[w :set nolist<CR>

map ]]t :tselect<CR>
